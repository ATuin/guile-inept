(use-modules (zip)
             ((zip ffi lzip)
              #:select (lzip/strerror))
             (gcrypt pk-crypto)
             (gcrypt-utils pk)
             (gcrypt-utils aes)
             (ice-9 match)
             (ice-9 getopt-long)
             (ice-9 binary-ports)
             (srfi srfi-26)
             (sxml simple)
             (sxml xpath))

(define *encryption-path* "META-INF/encryption.xml")
(define *rights-path* "META-INF/rights.xml")
(define *mimetype-path* "mimetype")
(define *xml-adept-prefix* "http://ns.adobe.com/adept:")
(define *adept-user-key-xpath* '(rights licenseToken encryptedKey))

(define (adept-xpath-symbols symbols)
  (map (lambda (s)
         (string->symbol (string-append *xml-adept-prefix* (symbol->string s))))
       symbols))

(define (ebook-file ebook fname)
  (let ((f (archive-file ebook fname)))
    (unless f
      (format (current-error-port) "Error opening ~a file: ~a~%" fname
              (lzip/strerror (archive-zip ebook))))
    f))

(define (ebook-fname->sxml ebook fname)
  (let* ((port (file-input-port (ebook-file ebook fname)))
         (sxml (xml->sxml port)))
    (close port)
    sxml))

(define (ebook-rights.xml ebook)
  (ebook-file ebook *rights-path*))

(define (ebook-rights.xml->sxml ebook)
  (let ((rights.xml (ebook-rights.xml ebook)))
    (and rights.xml (xml->sxml (file-input-port rights.xml)))))

(define (ebook-encryption.xml->sxml ebook)
  (ebook-fname->sxml ebook *encryption-path*))

(define (ebook-mimetype ebook)
  (ebook-file ebook *mimetype-path*))

(define (ebook-encrypted? ebook)
  (and (archive-file? ebook *encryption-path*)
       (archive-file? ebook *rights-path*)))

(define (ebook-user-key ebook rsa-key)
    (let* ((rights-sxml (ebook-rights.xml->sxml ebook))
           (path (adept-xpath-symbols *adept-user-key-xpath*))
           (enc-user-key (car ((sxpath `(,@path *text*)) rights-sxml))))
      (cadr (canonical-sexp->sexp (decrypt (base64->pkcs1-decrypt-sexp enc-user-key)
                                           rsa-key)))))

(define (ebook-encrypted-files ebook)
  (let ((encrypted-files (make-hash-table)))
    (for-each (lambda (p)
                (hash-set! encrypted-files (cadr p) #t))
              ((sxpath '(// (URI))) (ebook-encryption.xml->sxml ebook)))
    encrypted-files))

(define (ebook-encrypted-file->source file key)
  (with-file-input-port (port file)
    (with-aes (aes key)
      (let ((bv (inflate->bytevector (aes-decrypt aes (get-bytevector-all port))
                                     #:offset 16
                                     #:raw-inflate #t)))
        (bytevector->source bv)))))

(define (ebook-add-file ebook file key encrypted-files)
  (let* ((path (file-name file))
         (encrypted? (hash-ref encrypted-files (file-name file)))
         (source (if encrypted? (ebook-encrypted-file->source file key)
                     (zip-file->source file))))
    (unless (member path `(,*mimetype-path* ,*rights-path* ,*encryption-path*))
      (format #t "Adding file: ~a~a~%" path (or (and encrypted? " [decrypting]") ""))
      (archive-add-source! ebook path source #:cm 'CM-DEFLATE))))

(define (inept key-file in-ebook out-ebook)
  (with-archive (in-ebook in-ebook '(ZIP-RDONLY))
    (unless (ebook-encrypted? in-ebook)
      (error "epub does not look encrypted!"))
    (let ((user-key (ebook-user-key in-ebook (read-file-sexp key-file)))
          (encrypted-files (ebook-encrypted-files in-ebook)))
      (with-archive (out-ebook out-ebook '(ZIP-CREATE))
        (format #t "Adding mimetype~%")
        (archive-add-source! out-ebook "mimetype"
                             (zip-file->source
                              (archive-file in-ebook "mimetype"))
                             #:cm 'CM-STORE)
        (for-each (cut ebook-add-file out-ebook <> user-key encrypted-files)
                  (archive-files in-ebook))))))

(define (check-dedrm ebook path)
  (format #t "Checking ebook~%")
  (with-archive (ebook ebook '(ZIP-RDONLY))
    (let lp ((files (archive-files ebook)))
      (when (pair? files)
        (with-file-input-port (p (car files))
          (with-input-from-file (string-append path (file-name (car files)))
            (lambda ()
              (let* ((bv1 (get-bytevector-all p))
                     (bv2 (get-bytevector-all (current-input-port)))
                     (same? (array-equal? bv1 bv2)))
                (when (not same?)
                  (format #t "Different: ~a~%" (file-name (car files))))))))
        (lp (cdr files))))))

(define options-spec
  '((help (single-char #\h) (value #f))))

(define (help)
  "Show help"
  (format #t "Inept options key infile outfile~%"))

(define (main args)
  (let* ((options (getopt-long args options-spec)))
    (if (option-ref options 'help #f)
        (help)
        (match-let (((key in-ebook out-ebook) (cdar options)))
          (inept key in-ebook out-ebook)))))
          ;; (check-dedrm out-ebook)))))
